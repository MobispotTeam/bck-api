# -*- coding: utf-8 -*-
import logging

from web.database import Model, SurrogatePK, db, ReferenceCol, relationship
from web.helpers import date_helper

logger = logging.getLogger(__name__)


class Person(SurrogatePK, Model):

    __tablename__ = 'person'

    PER_PAGE = 50
    MAX_PER_PAGE = 1000

    STATUS_VALID = 1
    STATUS_BANNED = 0

    TYPE_TIMEOUT = 0
    TYPE_WALLET = 1

    name = db.Column(db.Text, nullable=False)
    tabel_id = db.Column(db.String(150))
    birthday = db.Column(db.Date())
    firm_id = ReferenceCol('firm', nullable=False)
    firm = relationship('Firm', backref='person')
    card = db.Column(db.String(8))
    payment_id = db.Column(db.String(20), nullable=False, index=True)
    hard_id = db.Column(db.String(128), nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.Integer, nullable=False, index=True)
    wallet_status = db.Column(db.Integer, nullable=False, index=True)
    type = db.Column(db.Integer, nullable=False, index=True)
    manually_blocked = db.Column(db.Integer)

    def __init__(self):
        self.status = self.STATUS_VALID
        self.wallet_status = self.STATUS_VALID
        self.type = self.TYPE_TIMEOUT
        self.creation_date = date_helper.get_current_date()
        self.name = u'Пользователь'
        manually_blocked = self.STATUS_VALID

    @staticmethod
    def delete(person):
        try:
            db.session.query(PersonEvent).\
                filter(PersonEvent.person_id == person.id).delete()
            db.session.query(PersonWallet).\
                filter(PersonWallet.person_id == person.id).delete()
            db.session.delete(person)
            db.session.commit()
        except Exception, e:
            db.session.rollback()
            logger.error(e)
            return False

        return True

    def to_dict(self):
        result = {
            'ID': self.id,
            'CardID': self.card,
            'Name': self.name,
            'TabelID': self.tabel_id,
            'Status': bool(self.status),
            'WalletStatus': bool(self.wallet_status),
        }
        person_wallet = {'Balance': 0}
        if len(self.corp_wallet) > 0:
            person_wallet = {'Balance': self.corp_wallet[0].balance}

        result.update(person_wallet)
        return result

    def get_status(self):
        if not self.payment_id:
            return self.STATUS_BANNED

        if self.manually_blocked == self.STATUS_BANNED:
            return self.STATUS_BANNED

        person_event = PersonEvent.query.filter(
            PersonEvent.person_id == self.id).first()

        if not person_event:
            return self.STATUS_BANNED

        corp_wallet = PersonWallet.query.filter(
            PersonWallet.person_id == self.id).first()

        if self.type == self.TYPE_WALLET and not corp_wallet:
            return self.STATUS_BANNED

        if self.type == self.TYPE_WALLET and corp_wallet.balance < PersonWallet.BALANCE_MIN:
            return self.STATUS_BANNED

        return self.STATUS_VALID
        
    def save(self):
        self.status = self.get_status()
        return Model.save(self)
        
        
class PersonEvent(SurrogatePK, Model):

    __tablename__ = 'person_event'

    STATUS_ACTIVE = 1
    STATUS_BANNED = 0

    person_id = ReferenceCol('person', nullable=False)
    person = relationship('Person', backref='person_event')
    term_id = ReferenceCol('term', nullable=False)
    term = relationship('Term', backref='person_event')
    event_id = ReferenceCol('event', nullable=False)
    event = relationship('Event', backref='person_event')
    firm_id = ReferenceCol('firm', nullable=False)
    firm = relationship('Firm', backref='person_event')
    timeout = db.Column(db.Integer, nullable=False)
    status = db.Column(db.Integer, nullable=False)


class PersonWallet(SurrogatePK, Model):

    __tablename__ = 'corp_wallet'
    
    BALANCE_MIN = 40
    
    STATUS_DISABLED = 0
    STATUS_ACTIVE = 1
    STATUS_BANNED = -1

    person_id = ReferenceCol('person', nullable=False)
    person = relationship('Person', backref='corp_wallet')
    creation_date = db.Column(db.DateTime, nullable=False)
    balance = db.Column(db.Integer, nullable=False)
    limit = db.Column(db.Integer, nullable=False)
    interval = db.Column(db.Integer, nullable=False, index=True)
    status = db.Column(db.Integer, nullable=False, index=True)

    def set_balance(self, new_balance):
        self.balance = new_balance
        person = Person.query.get(self.person_id)
        if not person:
            return self.save(self)
        if self.balance < PersonWallet.BALANCE_MIN:
            person.wallet_status = Person.STATUS_BANNED
        else:
            person.wallet_status = person.STATUS_VALID
            self.status = PersonWallet.STATUS_ACTIVE
            
        person.save()
        
        return self.save(self)